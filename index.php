<?php require "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Array Manipulation </title>
</head>
<body>
	<h2>Task #1</h2>
	<pre>
	<?php 
	createProduct("Papaya ", 80.00);
	createProduct("Avocado ", 100.00); 
	createProduct("Orange ", 35.00); 
	print_r($products);
	?>

	</pre>
	<h2>Task #2</h2>	
	<ul>
	<?php printProducts($products); ?>
	</ul>
	
	<h2>Task #3</h2>
	<?php countProducts($products); ?>

	<h2>Task #4</h2>
	<?php deleteProducts($products); ?>

</body>
</html>