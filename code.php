<!-- Array manipulation

1. Create a function called 'createProduct' that accepts a name of the product and price as its parameter and add it to an array called 'products'

2. Create a function called 'printProducts' that displays the lists of elements of products array on an unordered list in the index.php

3. Create a function called 'countProducts' that display the count of the elements on a products array in the index.php

4. Create a function called 'deleteProduct' that deletes an element to a products array.
 -->
 <?php 

$products = [];

function createProduct($name, $price){
	global $products;
	array_push($products, ['Name' => $name, 'Price' => $price]);
}

function printProducts() {
	global $products;

	foreach($products as $product){
		echo"<li>";
		foreach($product as $label => $value){
			echo "$label:$value";
		}
		echo "</li>";
	}
}

//Count Products
function countProducts($productList){
	$count = count($productList);
	echo "Total number of products: $count";
}

//Delete Products
function deleteProducts($productList){
	array_pop($productList);
	$count = count($productList);
	echo "Total number of products: $count";
}


?>

<?php

// $products = [
// 	'Apple' => 35,
// 	'Orange' => 30,
// 	'Strawberry' => 150,
// 	'Banana' => 100
// ];

//createProduct
// function createProduct($name, $price){
// 		global $products;
//		$products['$name'] = $price;
// 	}
	

//Display Products
// function printProducts($productList){
// 	foreach($productList as $productName => $price){
// 		echo "<li> $productName => $price </li>";
// 	}
// }

//Count Products
// function countProducts($productList){
// 	$count = count($productList);
// 	echo "Total number of products: $count";
// }

//Delete Products
// function deleteProducts($productList){
// 	array_pop($productList);
// 	$count = count($productList);
// 	echo "Total number of products: $count";
// 	foreach($productList as $productName => $price){
// 		echo "<li> $productName => $price </li>";
// 	}
// }


?>

